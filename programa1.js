//Iteration 1: Names and Input (Leer los nombres de Navigator y Driver )
//================================================
let hacker1="Jean";
let hacker2="Jeniffer";

console.log("The driver's name is "+hacker1);
console.log("The navigator's name is "+hacker2);


//Iteration 2: Conditionals 
//================================================
if(hacker1.length === hacker2.length){
  console.log("Wow, you both have equally long names: "+ hacker1.length +  "characters!");
}else if(hacker1.length > hacker2.length){
console.log("The driver has the longest name, it has "+ hacker2.length +  " characters");
}else{
  console.log("It seems that the navigator has the longest name, it has " + hacker2.length +" characters");
}

//Iteration 3: Loops
//==========================================
hacker1 = hacker1.toUpperCase();

let hacker = hacker1.split("").join(" ");
console.log(hacker);

let nameOrder = "";
for (i = hacker2.length -1; i >= 0; i--) {
    nameOrder += hacker2[i];
}
console.log(nameOrder);

let consultaLex=hacker1.localeCompare(hacker2);
 if (consultaLex === 0){
   console.log("What?! You both have the same name?\n\n")
 }else if(consultaLex === -1){
    console.log("The driver's name goes first.\n\n")
 }else {
    console.log("Yo, the navigator goes first definitely.\n\n");
  }
//Iteration 4: counts words
//==========================================
const lorem="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam suscipit semper leo. Donec malesuada urna sed libero fringilla, sit amet viverra arcu pulvinar. Phasellus pellentesque lacinia ligula, ut sollicitudin ipsum tempus et. Sed tincidunt tincidunt nisl in sollicitudin. Pellentesque et quam accumsan, vulputate elit eget, congue elit. Aliquam erat volutpat. Nullam nec velit vitae turpis laoreet faucibus. Fusce maximus vestibulum blandit. Mauris consequat volutpat sodales.\n Donec ipsum tortor, semper et mattis eget, tristique sit amet nisl. Fusce efficitur facilisis rhoncus. Maecenas a sem tristique, placerat ante nec, efficitur mauris. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse cursus nunc a feugiat dignissim. Suspendisse vitae nisi sit amet urna hendrerit fermentum quis at sem. Nulla sagittis elementum nisl, vel malesuada lectus.\n Morbi sed felis felis. Pellentesque lorem turpis, pharetra ac ligula hendrerit, finibus luctus dolor. In velit ipsum, ullamcorper aliquet quam a, vehicula consectetur nisl. Nunc elementum urna ut orci aliquet, non consequat sem ultricies. Etiam felis nisi, hendrerit in nibh id, feugiat aliquam nibh. Proin dui orci, malesuada sed bibendum in, placerat vel lacus. Maecenas cursus eget lorem non faucibus. Fusce ultricies, dui sed condimentum sodales, leo magna ullamcorper eros, sit amet interdum ipsum est sed mauris. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut felis lacus, dignissim at nisl vitae, dictum placerat mi. Nunc dignissim lobortis velit, ac commodo eros varius lacinia. Nam vestibulum magna augue, id dictum arcu tincidunt dignissim. Fusce eu justo eleifend, imperdiet ex vestibulum, bibendum ex. Sed porta, risus vitae volutpat ultricies, dui justo blandit ipsum, lobortis dignissim turpis diam eget magna. Maecenas commodo, augue fringilla pretium tempor, purus mi venenatis nisl, at imperdiet urna justo id sapien.";

let textWord = lorem.split(" ");
console.log("Las palabras contadas son: "+ textWord.length);

let countEt = 0
for (i = 0; i < textWord.length; i++) {
    if (textWord[i].includes("et")) {
        countEt++;
    }

}
console.log("Cantidad de veces que aparece et: "+countEt);

//Iteration 5: Palindrome without array methods
//=====================================
let phraseCheck = "A man, a plan, a canal, Panama!";
let guardaCade= "";
let guardaCade2= "";
function compara (list){
  for (let i = phraseCheck.length -1 ; i >= 0; i-- ){
    let caracter = list.charAt(i);
     caracter  = caracter.toUpperCase();
    switch (caracter){
      case '!':
        caracter = "";
        break;
      case '?':
        caracter = "";
        break;
      case ',':
        caracter = "";
        break;
      case ' ':
        caracter = "";
        break;
      case "'":
        caracter = "";
        break;
      default:
        guardaCade += caracter;
        break;
    }
  }
}

function compara2 (list){
   for (let i = 0; i < list.length; i++ ){
    let caracter2 = list.charAt(i);
     caracter2  = caracter2.toUpperCase();
    switch (caracter2){
      case '!':
        caracter = "";
        break;
      case '?':
        caracter = "";
        break;
      case ',':
        caracter = "";
        break;
      case ' ':
        caracter = "";
        break;
      case "'":
        caracter = "";
        break;
      default:
        guardaCade2 += caracter2;
        break;
    }
  }
}

compara(phraseCheck);
compara2(phraseCheck);

let consultaCad = guardaCade.localeCompare(guardaCade2);
if(consultaCad===0){
  console.log("Esto es un palindromo");
}
else{console.log("Esto no es un palindromo");}